import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';

export const StyledListItem = styled(ListItem)({
	border: '.15rem solid #c1cfda',
	marginBottom: '1rem',
});

export const StyledListItemText = styled(ListItemText)({
	paddingBottom: '1.50rem',
})

export const StyledDate = styled('span')({
	position: 'absolute',
	display: 'block',
	right: '1rem',
	fontStyle: 'italic',
	fontSize: '1rem',
	fontColor: '#273846',
});


export const StyledMissionTitle = styled(Typography)({
	fontWeight: 'bold',
	fontSize: '1.15rem',
});