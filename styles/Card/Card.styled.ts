import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';

export const StyledCard = styled(Card)({
	marginTop: '6rem',
	border: '.15rem solid #c1cfda',
});

export const StyledButton = styled(Button)({
	display: 'block',
	margin: '1rem auto 0',
	textTransform: 'none',
	color: '#273846',
	background: '#c1cfda',

	'&:hover': {
		background: '#c1cfda',
	}
});
