import { gql } from 'apollo-boost';

const GET_MISSION = gql`
	query GetMission($id: ID!) {
		mission(id: $id) {
			description
	    id
	    name
	    wikipedia
		}
	}
`;

export default GET_MISSION;
