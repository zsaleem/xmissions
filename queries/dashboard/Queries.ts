import { gql } from 'apollo-boost';

const GET_MISSIONS = gql`
	query GetMissions($limit: Int) {
		missions(limit: $limit) {
			description
	    id
	    name
		}
	}
`;

export default GET_MISSIONS;
