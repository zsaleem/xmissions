interface ICardData {
	description: string;
	id: string;
	name: string;
	wikipedia: string;
}

export interface ICardProps {
	data: ICardData;
}
