export interface ITruncateInputs {
	description: string;
	maxChars: number;
}

export interface IListData {
	id: string;
	description: string;
	name: string;
}

export interface IListItemProps {
	data: IListData[];
}
