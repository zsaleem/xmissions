## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `xmissions` folder.
    3. Run `npm install` command to download and install all dependencies.
    4. To run this project use `npm run dev` command in command line.

## Stack
 - NextJS
 - React
 - React Hooks
 - Typescript
 - Material UI
 - Apollo Client
 - JSX
 - Swimlanes.io
 - Git
 - Gitlab
 - Gitlab CI/CD
 - Heroku
 - NPM
 - Sublime Text
 - Mac OS X
 - Google Chrome Incognito
 - nodejs(16.9.1)

## Live Demo
To view live demo if you would like to test it on production then please [click here](https://xmissions.herokuapp.com/).

To view a video where I show working demo then [click here](https://youtu.be/sUFLZlkPxs8).

To view list of CI/CD pipelines [click here](https://gitlab.com/zsaleem/xmissions/-/pipelines).

To view list of all commits [click here](https://gitlab.com/zsaleem/xmissions/-/commits/develop).

To view list of all branches [click here](https://gitlab.com/zsaleem/xmissions/-/branches/all).

To view list of all merge requests [click here](https://gitlab.com/zsaleem/xmissions/-/merge_requests?scope=all&state=all).

To view git branch strategy [click here](https://swimlanes.io/u/hVC229Ws7).

## Disclaimer
1. Since NextJS routing system use `pages/` folder to redirect users from one page to another for that reason adding any pages to this folder becomes part of routing for that reason I put non-page components and files such as `interfaces/`, `queries/`, `styles/` etc associated with their pages outside of `pages/` folder.

2. Since the SpaceX GraphQL API does not return launch date while calling `missions` endpoint for that reason I hardcoded the date inside the component.
