import type { NextPage } from 'next';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useLazyQuery } from '@apollo/react-hooks';
import Layout from '../../components/Layout/';
import Loader from '../../components/Loader/';
import Card from './components/Card/';
import GET_MISSION from '../../queries/mission/';

const Mission: NextPage = () => {
	const router = useRouter();
  const { MissionID } = router.query;
  const [getMission, mission] = useLazyQuery(GET_MISSION);

  useEffect(() => {
  	if (MissionID) {
  		getMission({
	  		variables: {
					id: MissionID,
				}
	  	});
  	}
  }, [MissionID]);

	return (
		<Layout appTitle={`${mission.data?.mission.name} | Space X`}>
			{
				mission.loading
				?
				<Loader />
				:
				<Card data={mission.data?.mission} />
			}
		</Layout>
	);
};

export default Mission;
