import type { NextPage } from 'next';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { ICardProps } from '../../../../interfaces/Card/interfaces';
import {
	StyledCard,
	StyledButton,
} from '../../../../styles/Card/Card.styled';

const Card: NextPage<ICardProps> = ({ data }) => {
	return (
		<StyledCard elevation={0} square={true}>
      <CardMedia
        component="img"
        height="512"
        image={`https://ui-avatars.com/api/?name=${data?.name}&size=512&background=random`}
        alt={data?.name}
      />
      <CardContent>
      	<Typography
      		component="h1"
        	variant="h4"
        	color="text.primary"
        	align="center"
        >
          {data?.name}
        </Typography>
        <Typography
        	variant="body2"
        	color="text.primary"
        	align="center"
        	marginTop="1rem"
        >
          {data?.description}
        </Typography>
        <StyledButton
		      variant="contained"
		      onClick={() => window.location.assign(data?.wikipedia)}
		    >
		      See more
		    </StyledButton>
      </CardContent>
    </StyledCard>
	);
}

export default Card;
