import type { NextPage } from 'next';
import { useEffect } from 'react';
import List from '@mui/material/List';
import { useLazyQuery } from '@apollo/react-hooks';
import Layout from '../../components/Layout/';
import Title from '../../components/Title/';
import Loader from '../../components/Loader/';
import ListItem from './components/ListItem/';
import GET_MISSIONS from '../../queries/dashboard/';

const Dashboard: NextPage = () => {
	const limit: number = 10;
	const [getMissions, missions] = useLazyQuery(GET_MISSIONS);

	useEffect(() => {
		getMissions({
			variables: {
				limit,
			}
		});
	}, []);

	return (
		<Layout appTitle="SpaceX list of missions" loading={missions.loading}>
			<Title label="Latest Launches" icon={true} />
			{
				missions.loading
				?
				<Loader />
				:
				<List dense sx={{ width: '100%', maxWidth: '100%' }}>
		      <ListItem
		      	data={missions.data?.missions}
		      />
		    </List>
			}
		</Layout>
	);
};

export default Dashboard;
