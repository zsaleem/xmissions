import type { NextPage } from 'next';
import Link from 'next/link';
import ListItemButton from '@mui/material/ListItemButton';
import Typography from '@mui/material/Typography';
import {
	IListItemProps,
	ITruncateInputs,
	IListData,
} from '../../../../interfaces/ListItem/interfaces';
import {
	StyledListItem,
	StyledDate,
	StyledListItemText,
	StyledMissionTitle,
} from '../../../../styles/ListItem/ListItem.styled';

const ListItem: NextPage<IListItemProps> = ({ data }) => {
	const truncateDescription = ({ description, maxChars }: ITruncateInputs) => {
	  if (description.length <= maxChars) {
	    return description;
	  }

	  return description.slice(0, maxChars) + '...';
	}

	return (
		<>
			{data?.map((mission: IListData) => {
        return (
          <StyledListItem
            key={mission.id}
            disablePadding
          >
            <Link href={`/mission/${mission?.id}`}>
            	<a>
            		<ListItemButton>
		              <StyledListItemText
					          primary={
				          		<StyledMissionTitle
				                variant="body2"
					            >
					              {mission.name}
					            </StyledMissionTitle>
					          }
					          secondary={
					            <>
					              <Typography
					                component="span"
					                variant="body2"
					                color="text.primary"
					              >
					              	{truncateDescription({ description: mission.description, maxChars: 240 })}
					              </Typography>
					              <StyledDate>
					              	20/04/2020
					              </StyledDate>
					            </>
					          }
					        />
				        </ListItemButton>
			        </a>
            </Link>
          </StyledListItem>
        )
      })}
		</>
	);
}

export default ListItem;
