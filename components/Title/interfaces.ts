export interface ITitleProps {
	label: string;
	icon?: boolean;
}
