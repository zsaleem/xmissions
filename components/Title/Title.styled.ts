import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';

export const StyledTitle = styled(Typography)({
	fontWeight: 'bold',
	fontSize: '2rem',
	marginTop: '6rem',
});
