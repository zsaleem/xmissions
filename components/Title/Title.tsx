import type { NextPage } from 'next';
import { StyledTitle } from './Title.styled';
import { ITitleProps } from './interfaces';

const Title: NextPage<ITitleProps> = ({
	label,
	icon = false,
}) => (
	<StyledTitle>
		{label} {icon ? <span>&#128640;</span> : ''}
	</StyledTitle>
);

export default Title;
