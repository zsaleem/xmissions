import type { NextPage } from 'next';
import Head from 'next/head';
import Container from '@mui/material/Container';
import Header from '../Header/';
import Footer from '../Footer/';
import { ILayoutProps } from './interfaces';

const Layout: NextPage<ILayoutProps> = (props) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="Gets list of space x missions!" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="SpaceX Missions" />
        <meta property="og:description" content="List of SpaceX Missions!" />
        <meta property="og:url" content="/" />
        <meta property="og:site_name" content="xMission" />
        <title>{props.appTitle}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Container maxWidth="lg">
	      <div className="Content">{props.children}</div>
      </Container>
      <Footer loading={props.loading} />
    </>
  );
}

export default Layout;
