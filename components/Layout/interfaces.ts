import React from 'react';

export interface ILayoutProps {
  children: React.ReactNode;
  appTitle: string;
  loading?: boolean;
};
