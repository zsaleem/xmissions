import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';

export const StyledFooter = styled(Typography)({
	display: 'block',
	padding: '1rem',
});
