import type { NextPage } from 'next';
import { IFooterProps } from './interfaces';
import { StyledFooter } from './Footer.styled';

const Footer: NextPage<IFooterProps> = ({ loading }) => {
	return (
		<StyledFooter
			align="center"
			mt={loading ? 60 : 0}
		>&copy; 2022</StyledFooter>
	);
};

export default Footer;
